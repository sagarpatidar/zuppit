package com.example.zuppit.general;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by MAD on 10/23/2015.
 */
public class Common {

    public static void setFontStyle(String font, AssetManager asset, TextView... views) {

        Typeface tf = Typeface.createFromAsset(asset, font);
        for(TextView v: views){
            v.setTypeface(tf);
        }

    }

    public static void showToast(Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void putLog(String message){
        Log.d("ZUPPIT DEBUG", message);
    }

    public static void setOnClickListener(View.OnClickListener onClickListener, View...views){
        for(View v: views){
            v.setOnClickListener(onClickListener);
        }
    }

    public static String longToDateString(long dateLong, String format) {
        Date date = new Date(dateLong);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

}
