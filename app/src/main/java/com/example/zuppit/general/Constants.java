package com.example.zuppit.general;

/**
 * Created by MAD on 10/23/2015.
 */
public class Constants {

    public static final String FONT_REGULAR = "fonts/calibri.ttf";

    public static final float TAN14 = 0.2493f;

    // Intent constants
    public static final String INTENT_POST_PICTURE = "post_pic";
    public static final String INTENT_POST_TAG = "post_tag";
    public static final String INTENT_POST_TITLE = "post_title";
    public static final String INTENT_POST_CONTENT = "post_content";
}
