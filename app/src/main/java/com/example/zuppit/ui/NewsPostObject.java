package com.example.zuppit.ui;

/**
 * Created by MAD on 10/23/2015.
 */
public class NewsPostObject {


    private int postId;
    private int imageResourceId;
    private String postTag;
    private String postTitle;
    private String postContent;

    public NewsPostObject(int mPostId, int mImageResourceId, String mPostTag, String mPostTitle, String mPostContent){
        this.postId = mPostId;
        this.imageResourceId = mImageResourceId;
        this.postTitle = mPostTitle;
        this.postContent = mPostContent;
        this.postTag = mPostTag;
    }


    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getPostTag() {
        return postTag;
    }

    public void setPostTag(String postTag) {
        this.postTag = postTag;
    }
}
