package com.example.zuppit.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.zuppit.general.Common;

import java.util.ArrayList;

/**
 * Created by MAD on 10/23/2015.
 */
public class LockScreenPageAdapter extends FragmentPagerAdapter {

    private ArrayList<NewsPostObject> posts;

    public LockScreenPageAdapter(FragmentManager fm) {
        super(fm);
    }
    public LockScreenPageAdapter(FragmentManager fm, ArrayList<NewsPostObject> mPosts){
        super(fm);
        this.posts = mPosts;
        Common.putLog("PagerAdapterSize: "+posts.size());
    }

    @Override
    public Fragment getItem(int index) {
        if(posts!=null && posts.size()>0){
            return new LockScreenPageFragment().newInstance(posts.get(index));
        }else {
            return null;
        }

    }

    @Override
    public int getCount() {
        if(posts!=null && posts.size()>0){
            return posts.size();
        }else{
            return 0;
        }
    }

}
