package com.example.zuppit.ui;

import android.os.Bundle;

import com.example.zuppit.Helper.MainActivity;
import com.example.zuppit.R;
import com.example.zuppit.customviews.CustomViewPager;
import com.example.zuppit.general.Common;

import java.util.ArrayList;

/**
 * Created by MAD on 10/23/2015.
 */
public class LockScreenActivity extends MainActivity {

    // User-interface
    private CustomViewPager viewPager;
    private ArrayList<NewsPostObject> postList;
    private LockScreenPageAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (CustomViewPager) findViewById(R.id.viewpager);

        postList = new ArrayList<NewsPostObject>();
        requestData();

        adapter = new LockScreenPageAdapter(getSupportFragmentManager(), postList);
        viewPager.setAdapter(adapter);

        viewPager.setOnSwipeOutListener(new CustomViewPager.OnSwipeOutListener() {
            @Override
            public void onSwipeOutAtEnd() {
                // You can use this to do whatever you want to do when user swipes right on last page.
                Common.putLog("On Swipe Out called");
            }
        });


    }

    private void requestData(){
        NewsPostObject post1 = new NewsPostObject(1, R.drawable.picture1, "World",
                "Refugees forced to scramble for food by police in Hungary",
                "Hungarian police are investigating a video showing police in surgical masks throwing packs of sandwiches to refugees.");

        NewsPostObject post2 = new NewsPostObject(2, R.drawable.picture2, "Politics",
                "Sadiq Khan elected as Labour's candidate for mayor of London",
                "Sadiq Khan, the former shadow justice secretary, has won the Labour nomination for the London mayoralty.");

        NewsPostObject post3 = new NewsPostObject(3, R.drawable.picture3, "Sports",
                "Eoin Morgan smashes 92 to help England level series with Australia",
                "For just the fourth time ever England chased down a target of 300 with their captain, Eoin Morgan, leading the way.");

        NewsPostObject post4 = new NewsPostObject(4, R.drawable.picture4, "India",
                "India won’t fire first bullet along border: Rajnath to Pak",
                "India will not fire the first bullet towards Pakistan as it wants cordial relations with all its neighbours.");

        NewsPostObject post5 = new NewsPostObject(5, R.drawable.picture5, "Entertainment",
                "Fatwa against A.R. Rahman for film on Prophet",
                "Objecting to an Iranian film, a Muslim group here has issued a fatwa against music composer A.R. Rahman.");

        NewsPostObject post6 = new NewsPostObject(6, R.drawable.picture6, "Business",
                "Gionee to invest $50 mn to make handsets in India",
                "Chinese smartphone maker Gionee will invest USD 50 million over next three years to make handsets in India.");

        postList.add(post1);
        postList.add(post2);
        postList.add(post3);
        postList.add(post4);
        postList.add(post5);
        postList.add(post6);


    }




}
