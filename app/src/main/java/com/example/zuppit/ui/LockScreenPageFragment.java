package com.example.zuppit.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.zuppit.R;
import com.example.zuppit.customviews.CustomDigitalClock;
import com.example.zuppit.general.Common;
import com.example.zuppit.general.Constants;

import java.util.Date;

/**
 * Created by MAD on 10/23/2015.
 */
public class LockScreenPageFragment extends Fragment {

    private ImageView picture;
    private LinearLayout buttonsGroup;
    private ImageView img1;
    private ImageView img2;
    private ImageView img3;
    private ImageView img4;


    public LockScreenPageFragment() {
    }

    public static LockScreenPageFragment newInstance( NewsPostObject post) {
        Bundle args = new Bundle();
        args.putInt(Constants.INTENT_POST_PICTURE, post.getImageResourceId());
        args.putString(Constants.INTENT_POST_TAG, post.getPostTag());
        args.putString(Constants.INTENT_POST_TITLE, post.getPostTitle());
        args.putString(Constants.INTENT_POST_CONTENT, post.getPostContent());
        LockScreenPageFragment feedFragment = new LockScreenPageFragment();
        feedFragment.setArguments(args);
        return feedFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        buttonsGroup = (LinearLayout) rootView.findViewById(R.id.lock_buttons_pad);
        picture = (ImageView) rootView.findViewById(R.id.lock_picture);
        img1 = (ImageView) rootView.findViewById(R.id.lock_button_img_1);
        img2 = (ImageView) rootView.findViewById(R.id.lock_button_img_2);
        img3 = (ImageView) rootView.findViewById(R.id.lock_button_img_3);
        img4 = (ImageView) rootView.findViewById(R.id.lock_button_img_4);
        TextView postDate = (TextView) rootView.findViewById(R.id.lock_date);
        TextView postDay = (TextView) rootView.findViewById(R.id.lock_day);
        CustomDigitalClock postTime = (CustomDigitalClock) rootView.findViewById(R.id.lock_time); // Deprecated but I am still using it to support lower devices
        TextView postTimeAmPm = (TextView) rootView.findViewById(R.id.lock_time_am_pm);
        TextView postTag = (TextView) rootView.findViewById(R.id.lock_scr_post_tag);
        TextView postTitle = (TextView) rootView.findViewById(R.id.lock_scr_post_title);
        TextView postContent = (TextView) rootView.findViewById(R.id.lock_scr_post_description);
        final View view1 = (View) rootView.findViewById(R.id.scr_view1);
        final View view2 = (View) rootView.findViewById(R.id.scr_view2);

        //setting the custom font
        Common.setFontStyle(Constants.FONT_REGULAR, getActivity().getAssets(),
                postTag, postTitle, postContent, postDate, postDay, postTime, postTimeAmPm);




        //setting date and time
        long timestamp = new Date().getTime();
        postDate.setText(Common.longToDateString(timestamp, "MMMM dd"));
        postDay.setText(Common.longToDateString(timestamp, "EEEE"));
        postTimeAmPm.setText(Common.longToDateString(timestamp, "a"));

        //setting the post variables
        Bundle bundle = getArguments();
        String tag, title, content;
        int pictureResource;
        if(bundle!=null){
            pictureResource = bundle.getInt(Constants.INTENT_POST_PICTURE,-1);
            tag = bundle.getString(Constants.INTENT_POST_TAG, "Tag");
            title = bundle.getString(Constants.INTENT_POST_TITLE, "Title");
            content = bundle.getString(Constants.INTENT_POST_CONTENT, "Content");
            postTag.setText(tag);
            postTitle.setText(title);
            postContent.setText(content);;
            picture.setImageResource(pictureResource);
            Common.putLog("LockScreenPageFragment Called");

        }else{
            Common.putLog("Oops! Getting null arguments.");

        }

        // to rotate the button's band about the hinge
        picture.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                picture.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                int[] locations = new int[2];
                int[] locationsButtonsPad = new int[2];
                picture.getLocationOnScreen(locations);
                buttonsGroup.getLocationOnScreen(locationsButtonsPad);
                int x = locations[0];
                int y = locations[1];
                System.out.println("position::: " + buttonsGroup.getHeight() + " " + y);

                //rotating button's pad
                Animation rotateAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
                buttonsGroup.setAnimation(rotateAnim);
                buttonsGroup.startAnimation(rotateAnim);

                //configuring edges of button's pad
                ViewGroup.LayoutParams params1 = view1.getLayoutParams();
                ;
                params1.width = (int) Math.round(Constants.TAN14 * buttonsGroup.getHeight()) + 8;
                params1.height = buttonsGroup.getHeight() + 1;
                Common.putLog(params1.width + " PARAM " + params1.height);

                ViewGroup.LayoutParams params2 = view2.getLayoutParams();
                ;
                params2.height = (int) Math.round(Constants.TAN14 * buttonsGroup.getHeight()) + 1;
                params2.width = buttonsGroup.getHeight() + 1;

                view1.setLayoutParams(params1);
                view2.setLayoutParams(params2);
                Animation rotateAnim1 = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate1);
                view1.setAnimation(rotateAnim1);
                view1.startAnimation(rotateAnim1);

                Animation rotateAnim2 = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate2);
                view2.setAnimation(rotateAnim2);
                view2.startAnimation(rotateAnim2);
            }
        });

        // rotate the icons back to bring to normal position
        img1.setRotation(-15f);
        img2.setRotation(-15f);
        img3.setRotation(-15f);
        img4.setRotation(-15f);

        //event listener
        Common.setOnClickListener(onClickListener, img4);

        return rootView;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.lock_button_img_4:{
                    Common.showToast(getActivity(), "Button 4");
                    ((LockScreenActivity)getActivity()).unlockHomeButton();
                    break;
                }
            }
        }
    };



}

